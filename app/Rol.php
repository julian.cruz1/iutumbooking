<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Rol extends Model
{
    //

    public function AddRol($id_user, $id_rol)
    {
        return DB::table('users_has_rols')->insert(
            array(
                'users_id' => $id_user,
                'rols_id' => $id_rol
            )
        );
    }
}
