<?php

namespace App\Http\Controllers;
use Session;
use App\User;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(){
        session_start();
        if(!empty($_SESSION)){
            if(empty(Session::get('rols'))){
                return view('welcome');
            }else{
                $rols = Session::get('rols');
                $mdUser = new User();
                $user = $mdUser->verify(Session::get('email'));
                return view('calendar')->with('user', $user);
            }
        }else{
            return redirect('api/cal');
        }

    }
    //


    public function logout(){
        Auth::logout();
        Session::flush();
        session_start();
        session_destroy();
        return redirect('/');
    }
}
