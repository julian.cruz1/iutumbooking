<?php

namespace App\Http\Controllers;
use App\User;
use Session;

use Illuminate\Http\Request;

class ChatController extends Controller
{

    public function index(){  
        session_start();
        if(empty(Session::get('rols'))){
            return view('welcome');
        }else{
          
                $rols = Session::get('rols');
                $mdUser = new User();
                $user = $mdUser->verify(Session::get('email'));
                if(count($rols) == 2) {
                    return view('chats')->with('user', $user);
                }else if(count($rols) == 1) {
                    return view('chats')->with('user', $user);;
                }   

              
        }   
    }
    //
}
