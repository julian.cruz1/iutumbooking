<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Laravel\Socialite\Facades\Socialite;
use App\User;
use App\Rol;
use Illuminate\Support\Facades\Hash;
use Session;
use Illuminate\Support\Facades\Auth;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

        /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from google.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        $user = Socialite::driver('google')->stateless()->user();
        $mdUser = new User();
        $mdRol = new Rol();
        $verify = $mdUser->verify($user->email);
        //Se crea nuevo si no se encontro
        if($verify->isEmpty()){
            $id = $mdUser->RegisterUser($user->name, $user->email, Hash::make($user->id), $user->avatar);
            $iutum = substr($user->email, -9);
            if($iutum == "iutum.com"){
                //Si es de iutum se le añaden los 2
                $addRol1 = $mdRol->AddRol($id, 1);
                $addRol2 = $mdRol->AddRol($id, 2);
                $rol = array(
                    'admin' => true,
                    'client' =>true,
                );
                Session::put('rols', $rol);
            }else{
                //Si es otro se le añade solo 1 que es cliente
                $addRol2 = $mdRol->AddRol($id, 2);
                $rol = array(
                    'client' =>true,
                ); 
                Session::put('rols', $rol);
            }
            Session::put('email', $user->email);
            Session::put('token', $user->token);
            Session::put('id', $user->id);
            return redirect('/home');
        }else{
            //Ya esta registrado
            $iutum = substr($verify[0]->email, -9);
            if($iutum == "iutum.com"){
                $rol = array(
                    'admin' => true,
                    'client' =>true,
                );
                Session::put('rols', $rol);
            }else{
                $rol = array(
                    'client' =>true,
                ); 
                Session::put('rols', $rol);
            }
            Session::put('user', 'Julian');
            Session::put('email', $user->email);
            Session::put('token', $user->token);
            Session::put('id', $user->id);
            $emails = Session::get('email');
            $rols = Session::get('rols');
            return redirect('/home');
        }
    }
}
