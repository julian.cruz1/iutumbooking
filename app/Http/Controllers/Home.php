<?php

namespace App\Http\Controllers;
use App\User;
use Session;

use Illuminate\Http\Request;

class Home extends Controller
{

    public function index(){
        session_start();
        if(!empty($_SESSION)){
            if(empty(Session::get('rols'))){
                return view('welcome');
            }else{
                    $rols = Session::get('rols');
                    $mdUser = new User();
                    $user = $mdUser->verify(Session::get('email'));
                    if(count($rols) == 2) {
                        return view('home')->with('user', $user);
                    }else if(count($rols) == 1) {
                        return view('home')->with('user', $user);;
                    }        
            }
        }    
        else{
            return redirect('api/cal');
        }    
    }
    //
}
