@extends('app')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 "><i class="ion ion-chatbox"></i> Chats</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                <li class="breadcrumb-item active">Chats</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- /.row -->
        <!-- Main row -->
        <div class="row p-md">
          <!-- Left col -->
         
          <section class="col-lg-12 connectedSortable" style="color:black">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>{{$user[0]->name}}</h2>
                </div>
            </div>
            <div class="row mt-3 row-chat" style="height:100px;">
                <div class="col-md-2 col-sm-2 align-middle text-center" style="height:100px;">
                    <img class="rounded-circle" src="{{$user[0]->avatar}}" style="height:100px" alt="">
                </div>
                <div class="col-md-7 col-sm-7 align-middle text-center" style="height:100px;border-bottom: 1px solid;">
                    {{$user[0]->name}} <br><br>
                    {{$user[0]->email}}<br><br>
                    2019/06/28
                </div>
                <div class="col-md-3 col-sm-3" style="height:100px;border-bottom: 1px solid;">
                    <button class="btn btn-primary btn-block" style="margin-top: 25px;">Ir al chat</button>
                </div>
            </div>
            <div class="row mt-3 row-chat" style="height:100px;">
                <div class="col-md-2 col-sm-2 align-middle text-center" style="height:100px;">
                    <img class="rounded-circle" src="{{$user[0]->avatar}}" style="height:100px" alt="">
                </div>
                <div class="col-md-7 col-sm-7 align-middle text-center" style="height:100px;border-bottom: 1px solid;">
                    {{$user[0]->name}} <br><br>
                    {{$user[0]->email}}<br><br>
                    2019/06/28
                </div>
                <div class="col-md-3 col-sm-3" style="height:100px;border-bottom: 1px solid;">
                    <button class="btn btn-primary btn-block" style="margin-top: 25px;">Ir al chat</button>
                </div>
            </div>
          </section>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @endsection
